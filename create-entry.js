const elJournals = document.getElementById('journal');
// const elEntryTitle = document.getElementById('title');
// const elEntryContent = document.getElementById('content');
const elAuthors = document.getElementById('authors');
// const elEntryCreated = document.getElementById('created');
// const elEntryUpdated = document.getElementById('updated');
const elTags = document.getElementById('tags');
const elTestButton = document.getElementById('test');
const elSaveButton = document.getElementById('save');
// let selectedAuthorId = 0;
let selectedTags = [];

function goBack() {
    window.history.back();
}

async function init() {
    try {
        const dataJournals = await fetch('http://localhost:3000/v1/api/journals').then(resp => resp.json());
        // Adding author options
        dataJournals.forEach(entry => {
            const elJournal = document.createElement('option');
            elJournal.innerHTML = entry.name;
            elJournal.value = entry.journal_id;
            elJournals.appendChild(elJournal);
        });
        // Can probably read value at submit instead of having an eventlistener here.
        elJournals.addEventListener("change", () => {
            // selectedJournalId = entry.author_id;
            console.log(elJournals.value);
        });
        const dataAuthors = await fetch('http://localhost:3000/v1/api/authors').then(resp => resp.json());
        // Adding author options
        dataAuthors.forEach(entry => {
            const elAuthor = document.createElement('option');
            elAuthor.innerHTML = entry.first_name + ' ' + entry.last_name;
            elAuthor.value = entry.author_id;
            elAuthors.appendChild(elAuthor);
        });
        // Can probably read value at submit instead of having an eventlistener here.
        elAuthors.addEventListener("change", () => {
            // selectedAuthorId = entry.author_id;
            console.log(elAuthors.value);
        });
        // Adding tag options
         const dataTags = await fetch('http://localhost:3000/v1/api/tags').then(resp => resp.json());
                // console.log(dataTags);
                dataTags.forEach(entry => {
                    // console.log(entry);
                    const elTagDiv = document.createElement('div');
                    elTagDiv.className = 'form-check';
                    const elTag = document.createElement('input');
                    elTag.type = 'checkbox';
                    elTag.value = entry.tag_id;
                    elTag.className = 'form-check-input';
                    const elTagName = document.createElement('label');
                    elTagName.innerHTML = entry.name;
                    elTagName.className = 'form-check-label';
                    elTag.addEventListener("change", () => {
                        if (elTag.checked) {
                            selectedTags.push(elTag.value);
                        } else {
                            selectedTags = selectedTags.filter( tags => tags != elTag.value )
                        }
                        selectedTags.sort();
                        // console.log(elTag.value);
                        // console.log(elTag.checked);
                    });
                    elTagDiv.appendChild(elTag);
                    elTagDiv.appendChild(elTagName);
                    elTags.appendChild(elTagDiv);
                });

        elSaveButton.addEventListener("click", () => {
            postEntry();
        });
        elTestButton.addEventListener("click", () => {
            console.log(selectedTags);
        });

    } catch (e) {
        alert(e.toString());
    }
}

let postEntry = () => {
    // let elTitle = document.getElementById('title').value;
    // let elContent = document.getElementById('content').value;
    let postData = {
        "title": document.getElementById('title').value,
        "content": document.getElementById('content').value,
        "journal_id": elJournals.value,
        "author_id": elAuthors.value
    };
    console.log(postData);
    fetch('http://localhost:3000/v1/api/entries', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(postData),
    })
        .then((response) => response.json())
        .then((postData) => {
            console.log('Success:', postData);
        })
        .catch((error) => {
            console.error('Error:', error);
        });

};