const elJournal = document.getElementById('journal');
const elEntryTitle = document.getElementById('title');
const elEntryContent = document.getElementById('content');
const elEntryAuthor = document.getElementById('author');
const elEntryCreated = document.getElementById('created');
const elEntryUpdated = document.getElementById('updated');
const elTags = document.getElementById('tags');
const entryId = document.location.search.slice(1);
let journal_id = 0;
let author_id = 0;

async function init() {
    // Get entry data
    try {
        const data = await fetch(`http://localhost:3000/v1/api/entries/${entryId}`).then(resp => resp.json());
        console.log( data );
        elEntryTitle.innerHTML = data.title;
        elEntryContent.innerHTML = data.content;
        elEntryCreated.innerHTML = 'Created: ' + data.created_at;
        if (data.updated != null) {
            elEntryUpdated.innerHTML = 'Updated: ' + data.updated_at;  
        }

        // Get journal belonging
        const journalData = await fetch(`http://localhost:3000/v1/api/journals/${data.journal_id}`).then(resp => resp.json());
        // console.log( journalData );
        elJournal.innerHTML = journalData.name;
        
        // Get author
        const authorData = await fetch(`http://localhost:3000/v1/api/authors/${data.author_id}`).then(resp => resp.json());
        // console.log( authorData );
        elEntryAuthor.innerHTML = authorData.first_name + ' ' + authorData.last_name;

        const tagData = await fetch(`http://localhost:3000/v1/api/entries/${entryId}/tags`).then(resp => resp.json());
        // console.log(tagData);
        tagData.forEach(tag => {
            // console.log(tag.name);
            const elTag = document.createElement('span');
            elTag.innerHTML = tag.name;
            elTag.className = 'badge badge-pill badge-dark mr-2';
            elTags.appendChild(elTag);
        });




  
    } catch (e) {
        alert(e.toString());
    }

}

function test() {
    console.log('hello');
}
