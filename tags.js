const elTags = document.getElementById('tags');
const elNewTag = document.getElementById('new-tag');
const elSaveTag = document.getElementById('save-tag');
const elModal = document.getElementById('createTag');

async function init() {
    try {
        const data = await fetch('http://localhost:3000/v1/api/tags').then(resp => resp.json());
        data.forEach(tag => {
            const elTag = document.createElement('tr');
            const elTagTitle = document.createElement('td');
            elTagTitle.innerHTML = tag.name;
            const elTagEmpty = document.createElement('td');
            elTagEmpty.colSpan = 4;
            elTag.appendChild(elTagTitle);
            elTag.appendChild(elTagEmpty);
            elTags.appendChild(elTag);
        });
        elSaveTag.addEventListener("click", function () {
            addTag(elNewTag.value);
            $('#createTag').modal('hide');
            console.log(elNewTag.value);
            // Updates page before database has been uppdated
            location.reload();
        });
    } catch (e) {
        alert(e.toString());
    }
}

let addTag = (name) => {
    // There is no creat tag api on the server yet
    let postData = {
        "name": name
    };
    console.log(postData);
    fetch('http://localhost:3000/v1/api/tags', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(postData),
    })
        .then((response) => response.json())
        .then((postData) => {
            console.log('Success:', postData);
        })
        .catch((error) => {
            console.error('Error:', error);
        });

};