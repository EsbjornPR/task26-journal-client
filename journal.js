const elJournals = document.getElementById('journals');
const elNewJournal = document.getElementById('new-journal');
const elSaveJournal = document.getElementById('save-journal');
const elModal = document.getElementById('createJournal');

async function init() {
    try {
        const data = await fetch('http://localhost:3000/v1/api/journals').then(resp => resp.json());
        data.forEach(journal => {
            const elJournal = document.createElement('tr');
            const elJournalTitle = document.createElement('td');
            elJournalTitle.innerHTML = journal.name;

            // const entryData = await fetch(`http://localhost:3000/v1/api/journals/${journal.journal_id}/entries`).then(resp => resp.json());
            const elJournalEntries = document.createElement('td');
            // elJournalEntries.innerHTML = entryData.length;
            const elJournalEmpty = document.createElement('td');
            elJournalEmpty.colSpan = 4;
            elJournal.appendChild(elJournalTitle);
            elJournal.appendChild(elJournalEntries);
            elJournal.appendChild(elJournalEmpty);
            elJournals.appendChild(elJournal);
        });
        elSaveJournal.addEventListener("click", function () {
            addJournal(elNewJournal.value);
            $('#createJournal').modal('hide');
            console.log(elNewJournal.value);
            // Updates page before database has been uppdated
            location.reload();
        });
    } catch (e) {
        alert(e.toString());
    }
}

let addJournal = (name) => {
    let postData = {
        "name": name
    };
    console.log(postData);
    fetch('http://localhost:3000/v1/api/journals', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(postData),
    })
        .then((response) => response.json())
        .then((postData) => {
            console.log('Success:', postData);
        })
        .catch((error) => {
            console.error('Error:', error);
        });

};



