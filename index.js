const elEntries = document.getElementById('entries');


//let lastSelection = 0;



async function init() {
    try {
        const data = await fetch('http://localhost:3000/v1/api/entries').then(resp => resp.json());
        data.forEach(entry => {
            const elEntry = document.createElement('tr');
            elEntry.addEventListener("click", function(){ 
                window.document.location = './entry.html?' + entry.journal_entry_id;
            });
            const elEntryTitle = document.createElement('th');
            elEntryTitle.innerHTML = entry.title;
            elEntryTitle.style.color = 'darkgrey';
            const elEntryContent = document.createElement('td');
            elEntryContent.innerText = entry.content;
            elEntryContent.colSpan = 2;
            const elEntryLink = document.createElement('td');
            elEntryLink.innerHTML = 'Delete';
            elEntryLink.style.textAlign = 'end';
 
            elEntry.appendChild(elEntryTitle);
            elEntry.appendChild(elEntryContent);
            elEntry.appendChild(elEntryLink);
            elEntries.appendChild(elEntry);
        });
        // const otherData = await fetch(.... bla bla
    } catch (e) {
        alert(e.toString());
    }

}



